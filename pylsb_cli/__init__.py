"""Python command-line interface for the Legacy Standard Bible.

Allows for easy command-line access to the LSB translation. All data is
downloaded from read.lsbible.org webpages (no API...), and cached for later
use.
"""

__version__ = '1.1.0'
